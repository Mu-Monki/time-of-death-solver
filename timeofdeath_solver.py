import math
from datetime import datetime

global ambient_temp
global body_temp1
global body_temp2
global time_measurement1
global time_measurement2
global time_diff
global hh
global mm
global ss
global coolingconst
global timeofdeath


def check_time_format(time_string):

    is_formatted = False
    if (len(time_string) == 8):
        hh = time_string[0:2]
        mm = time_string[3:5]
        ss = time_string[6:8]
        if (hh.isnumeric() and mm.isnumeric() and ss.isnumeric()):
            if (time_string[2] == ':' and time_string[5] == ':'):
                if (int(hh) < 24 and int(mm) < 60 and int(ss) < 60):
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False


def get_input():
    global ambient_temp
    global body_temp1
    global body_temp2
    global time_measurement1
    global time_measurement2
    global time_diff
    global hh
    global mm
    global ss

    isvalid = False
    ambient_temp = input("Input ambient temperature (Fahrenheit): ")
    while not is_float(ambient_temp):
        print("Invalid ambient temperature. Try again.")
        ambient_temp = input("Input ambient temperature: ")
    body_temp1 = input("Input first body temperature (Fahrenheit): ")
    while not is_float(body_temp1):
        print("Invalid body temperature. Try again.")
        body_temp1 = input("Input first body temperature: ")
    time_measurement1 = input(
        "Input the time you took the first measurement (HH:MM:SS 24H Format): ")
    isvalid = check_time_format(time_measurement1)
    while not isvalid:
        print("Invalid time format. Try again.")
        time_measurement1 = input(
            "Input the time you took the first measurement (HH:MM:SS 24H Format): ")
        isvalid = check_time_format(time_measurement1)
    body_temp2 = input("Input second body temperature (Fahrenheit): ")
    while not is_float(body_temp2):
        print("Invalid body temperature. Try again.")
        body_temp2 = input("Input second body temperature (Fahrenheit): ")
    time_measurement2 = input(
        "Input the time you took the second measurement (HH:MM:SS 24H Format): ")
    isvalid = check_time_format(time_measurement2)
    while not isvalid:
        print("Invalid time format. Try again.")
        time_measurement2 = input(
            "Input the time you took the second measurement (HH:MM:SS 24H Format): ")
        isvalid = check_time_format(time_measurement2)


def compute_timediff():
    global time_diff
    """
    time_diff = str(int(time_measurement2[0:2]) - int(time_measurement1[0:2])) + ":" + str(int(time_measurement2[3:5]) - int(
        time_measurement1[3:5])) + ":" + str(int(time_measurement2[6:8]) - int(time_measurement1[6:8]))"""
    time_diff = "%02d:%02d:%02d" % ((int(time_measurement2[0:2]) - int(time_measurement1[0:2])), (int(time_measurement2[3:5]) - int(
        time_measurement1[3:5])), (int(time_measurement2[6:8]) - int(time_measurement1[6:8])))


def is_float(arg):
    try:
        float(arg)
        return True
    except ValueError:
        return False


def parse_timediff():
    return float(time_diff[3:5]) + (float(time_diff[0:2]) * 60) + (float(time_diff[6:8]) / 60)


def compute_coolingconst():
    global coolingconst
    coolingconst = math.log((float(body_temp2) - float(ambient_temp)) /
                            (float(body_temp1) - float(ambient_temp))) / -parse_timediff()
    return coolingconst


def compute_timeofdeath():
    global timeofdeath
    mins = math.log((float(body_temp1) - float(ambient_temp)) / (98.6 - 70)) / -coolingconst
    hrs_sub = int(mins/60)
    mins_sub = int(mins % 60)
    hrs = int(time_measurement1[0:2]) - hrs_sub
    if (mins_sub > int(time_measurement1[3:5])):
        hrs = hrs - 1
        mins = 60 + int(time_measurement1[3:5]) - mins_sub
    else:
        mins = int(time_measurement1[3:5]) - mins_sub
    timeofdeath = str(hrs) + ":" + str(mins) + ":" + "00"
    print(timeofdeath)


# PROCESS FLOW
def process():

    get_input()
    compute_timediff()
    compute_coolingconst()
    compute_timeofdeath()

    print("\n\nDATA REPORT \n")
    print("\tAmbient Temperature: " + ambient_temp)
    print("\tBody temperature at " + time_measurement1 + ": " + body_temp1)
    print("\tBody temperature at " + time_measurement2 + ": " + body_temp2)
    print("\tTime difference between measurements: " + time_diff)
    print("\tCooling Constant: " + str(coolingconst))
    print("\n\tCONCLUSION: \n\tPerson died at approx " + timeofdeath)


# EXECUTION
process()
