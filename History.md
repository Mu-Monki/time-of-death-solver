### v0.0.0
    * Made the repository for the code
### v0.1
    * Made connection with reddit useing praw API
    * Allows the user to see the top 10 posts within a reddit sub
### v0.2
    *Changed the entire utility app to a forensics application
    *Computes the time of death using integration
    *Field validation added
    *Uses newton's law of cooling
    *Custom time parser function
    *Main body added
    *Temperature inputs assumed to be in Fahrenheit