import praw
import codecs

reddit = praw.Reddit(client_id=#INSERT PERSONAL CLIENT ID HERE,
                     client_secret=#INSERT PERSONAL CLIENT SECRET HERE,
                     username=#INSERT REDDIT USERNAME HERE,
                     password=#INSERT REDDIT PASSWORD HERE,
                     user_agent=#INSERT USER AGENT HERE)


# subredditStr = input('Type a Subreddit: ')
# if reddit.subreddit(subredditStr) is not None:
subredditStr = input('Type a Subreddit: ')

if reddit.subreddit(subredditStr) is not None:
    subreddit = reddit.subreddit(subredditStr)


hot = subreddit.hot(limit=10)

for element in hot:
    print('\nTITLE: {} \nUPVOTES: {} \nDOWNVOTES: {} \nVISITED: {}'.format
          (element.title.encode('utf-8'), element.ups, element.downs, element.visited))